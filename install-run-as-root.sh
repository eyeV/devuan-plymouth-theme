#Установка зависимостей
apt update
apt install -y sudo git grub-customizer plymouth plymouth-themes plymouth-x11 dialog

#Скачивает тему оформления brogit, источник - https://github.com/R0b1ns/brogit-plymouth-theme
git clone https://github.com/R0b1ns/brogit-plymouth-theme.git

#Открывает скачанную тему оформления
cd brogit-plymouth-theme
#Удаляет не нужные файлы и стандартные лого
rm -r background_example.png preview.png preview_example.png preview.sh logo.png debian.png

#Меняет название темы
sed -i  's/Name=brogit dark n clean Theme/Name=devuan/g' brogit-plymouth-theme.plymouth
#Меняет название папки темы
sed -i  's|ImageDir=/usr/share/plymouth/themes/brogit-plymouth-theme|ImageDir=/usr/share/plymouth/themes/devuan|g'         brogit-plymouth-theme.plymouth
#Меняет путь к теме:
sed -i  's|ScriptFile=/usr/share/plymouth/themes/brogit-plymouth-theme/brogit-plymouth-theme.script|ScriptFile=/usr/share/plymouth/themes/devuan/devuan.script|g' brogit-plymouth-theme.plymouth
#Меняет параметр высоты логотипа
sed -i  's|DEBIAN_HEIGHT_PCT = 0.07; # Debian image height in % of smallest screen dimension|DEBIAN_HEIGHT_PCT = 0.05; # Debian image height in % of smallest screen dimension|g' brogit-plymouth-theme.script

#Переименовывает скрипт темы и тему
mv brogit-plymouth-theme.plymouth devuan.plymouth
mv brogit-plymouth-theme.script   devuan.script

#Выходит из папки brogit-plymouth-theme в предыдущую
cd ..
#Копирует лого и надпись Devuan
cp -r dev-img/logo.png     brogit-plymouth-theme/logo.png
cp -r dev-img/debian.png   brogit-plymouth-theme/debian.png
cp -r dev-img/preview.png  brogit-plymouth-theme/preview.png

#Переименовывает папку на devuan
mv brogit-plymouth-theme   devuan

chmod -R u+rwx devuan
chmod -R o+rx  devuan

#Удаляет предыдущую версию если она есть
rm -r 		/usr/share/plymouth/themes/devuan
#Копирует тему в папку с темами plymouth
cp -r devuan    /usr/share/plymouth/themes/

#Устанавливает тему
sudo plymouth-set-default-theme devuan

#Добавляет параметр запуска анимации при включении в настройки grub
sed -i  's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/g' /etc/default/grub

#Применяет изменения в настройках grub
sudo update-grub
sudo update-grub2
sudo update-initramfs -u
