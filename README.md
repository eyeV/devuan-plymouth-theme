# Brogit plymouth theme для Devuan
**Зашифрованные системы не поддерживают эту тему**

Скрипт скачивает тему Debian, изменяет ее лого надпись и название на тему Devuan, затем устанавливает ее

![](https://codeberg.org/eyeV/devuan-plymouth-theme/raw/master/dev-img/preview.png)

Этот скрипт форкает [Brogit Plymouth Theme](https://github.com/R0b1ns/brogit-plymouth-theme)

